const { AkairoClient,
    CommandHandler,
    ListenerHandler } = require("discord-akairo");
const { token }       = require("./config.json")

class DamnBot extends AkairoClient {
    constructor() {
        super({
            ownerID: '262170998391046144'
        }, {
            disableMentions: 'everyone'
        });
        this.commandsrun = 0;
        
        this.commandHandler = new CommandHandler(this, {
            directory: './commands/',
            prefix: '='
        });

        this.commandHandler.loadAll();

        this.listenerHandler = new ListenerHandler(this, {
            directory: './listeners/'
        });

        this.commandHandler.useListenerHandler(this.listenerHandler);
        this.listenerHandler.setEmitters({
            commandHandler: this.commandHandler
        });

        this.listenerHandler.loadAll();
    }
}

const client = new DamnBot();
client.login(token);
