const { Command }      = require("discord-akairo");
const { MessageEmbed } = require("discord.js");
const SteamCommunity   = require("SteamCommunity");
const community        = new SteamCommunity;

class SteamAvatarCommand extends Command {
    constructor() {
        super('steamavatar', {
            aliases: ['steamavatar', 'stavatar'],
            args : [{
                id: "steamID",
                type: "string",
                prompt: {
                    start: "Please type the steamID to get the avatar."
                }
            }]
        });
    }

    exec(message, args) {
        community.getSteamUser(args.steamID, (err, user) => {
            if (err) {
                return message.channel.send("An error has occurred.\n");
            }
            const embed = new MessageEmbed();
            embed.setTitle(`${user.name}'s Avatar`);
            embed.setImage(user.getAvatarURL("full"));
            return message.channel.send(embed);
        });
    }

}

module.exports = SteamAvatarCommand;
