const { Command }      = require("discord-akairo");
const { MessageEmbed } = require("discord.js");
const { mstohuman }    = require("../utils/utils.js");

class InfoCommand extends Command {
    constructor() {
        super('info', {
            aliases: ['info', 'botinfo', 'bot']
        });
    }

    exec(message) {
        const client = this.client;
        const embed  = new MessageEmbed();
        embed.setTitle(`${client.user.username}'s Info'`)
            .setThumbnail(client.user.displayAvatarURL())
            .addField("Uptime", mstohuman(client.uptime))
            .addField("Commands Run", client.commandsrun)
            .addField("In ", `${client.guilds.cache.size} servers`, true)
            .addField("With ", `${client.users.cache.size} users`, true);
        return message.channel.send(embed);
    }
};

module.exports = InfoCommand;
