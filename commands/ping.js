const { Command } = require('discord-akairo');

class PingCommand extends Command {
    constructor() {
        super('ping', {
            aliases: ['ping', 'rs']
        });
    }

    async exec(message) {
        const m = await message.channel.send('Getting ping...');
        return m.edit(`Current ping is ${m.createdTimestamp - message.createdTimestamp} ms`);
    }
}

module.exports = PingCommand;
