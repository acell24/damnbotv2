const { Listener } = require("discord-akairo");

class CommandFinishedListener extends Listener {
    constructor() {
        super('commandFinished', {
            emitter: 'commandHandler',
            event: 'commandFinished'
        });
    }
    exec() {
        this.client.commandsrun+=1;
    }
}

module.exports = CommandFinishedListener;
