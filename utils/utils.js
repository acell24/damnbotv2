module.exports = {
    mstohuman(ms) {
        let d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        let string = "";
        if (d) {
            string += `${d} day${d > 1 ? "s" : ""}, `
        }
        if (h) {
            string += `${h} hour${h > 1 ? "s" : ""}, `
        }
        if (m) {
            string += `${m} minute${m > 1 ? "s" : ""}, `
        }
        if (s) {
            string += `${s} second${s > 1 ? "s" : ""}`
        }
        return string;
    }
};
